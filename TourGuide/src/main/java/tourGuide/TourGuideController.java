package tourGuide;

import java.util.ArrayList;
import java.util.List;

import dto.AttractionDto;
import dto.VisitedLocationDto;
import gpsUtil.location.Attraction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jsoniter.output.JsonStream;

import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tourGuide.user.User;
import tripPricer.Provider;

@RestController
public class TourGuideController {

    @Autowired
    TourGuideService tourGuideService;

    @Autowired
    RewardsService rewardsService;

    @Autowired
    RewardCentral rewardCentral;

    @RequestMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }

    @RequestMapping("/getLocation")
    public String getLocation(@RequestParam String userName) {
        VisitedLocation visitedLocation = tourGuideService.getUserLocation(getUser(userName));
        return JsonStream.serialize(visitedLocation.location);
    }

    //  TODO: Change this method to no longer return a List of Attractions.
    //  Instead: Get the closest five tourist attractions to the user - no matter how far away they are.
    //  Return a new JSON object that contains:
    // Name of Tourist attraction,
    // Tourist attractions lat/long,
    // The user's location lat/long,
    // The distance in miles between the user's location and each of the attractions.
    // The reward points for visiting each Attraction.
    //    Note: Attraction reward points can be gathered from RewardsCentral
    @RequestMapping("/getNearbyAttractions")
    public String getNearbyAttractions(@RequestParam String userName) {
        VisitedLocation visitedLocation = tourGuideService.getUserLocation(getUser(userName));
        List<Attraction> fiveAttractions = tourGuideService.getNearByAttractions(visitedLocation);
        List<AttractionDto> attractionDtoList = new ArrayList<>();
        for (Attraction attraction : fiveAttractions) {
            double distance = rewardsService.getDistance(attraction, visitedLocation.location);
            int rewardPoint = rewardCentral.getAttractionRewardPoints(attraction.attractionId, visitedLocation.userId);
            AttractionDto attractionDto = new AttractionDto();
            attractionDto.setAttractionName(attraction.attractionName);
            attractionDto.setLatitudeAttractionLocation(attraction.latitude);
            attractionDto.setLongitudeAttractionLocation(attraction.longitude);
            attractionDto.setLatitudeUserLocation(visitedLocation.location.latitude);
            attractionDto.setLongitudeUserLocation(visitedLocation.location.longitude);
            attractionDto.setDistanceBetweenUserAndAttraction(distance);
            attractionDto.setRewardsPointOfAttraction(rewardPoint);
            attractionDtoList.add(attractionDto);
        }
        return JsonStream.serialize(attractionDtoList);
    }

    @RequestMapping("/getRewards")
    public String getRewards(@RequestParam String userName) {
        return JsonStream.serialize(tourGuideService.getUserRewards(getUser(userName)));
    }

    @RequestMapping("/getAllCurrentLocations")
    public String getAllCurrentLocations(@RequestParam String userName) {
        // TODO: Get a list of every user's most recent location as JSON
        //- Note: does not use gpsUtil to query for their current location,
        //        but rather gathers the user's current location from their stored location history.
        //
        // Return object should be the just a JSON mapping of userId to Locations similar to:
        //     {
        //        "019b04a9-067a-4c76-8817-ee75088c3822": {"longitude":-48.188821,"latitude":74.84371}
        //        ...
        //     }
        VisitedLocationDto visitedLocationDto = new VisitedLocationDto();
        User user = tourGuideService.getUser(userName);
        VisitedLocation visitedLocation = user.getLastVisitedLocation();
        visitedLocationDto.setUserId(user.getUserId());
        visitedLocationDto.setLatitude(visitedLocation.location.latitude);
        visitedLocationDto.setLongitude(visitedLocation.location.longitude);

        return JsonStream.serialize(visitedLocationDto);
    }

    @RequestMapping("/getTripDeals")
    public String getTripDeals(@RequestParam String userName) {
        List<Provider> providers = tourGuideService.getTripDeals(getUser(userName));
        return JsonStream.serialize(providers);
    }

    private User getUser(String userName) {
        return tourGuideService.getUser(userName);
    }


}