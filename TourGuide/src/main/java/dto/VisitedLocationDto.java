package dto;

import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class VisitedLocationDto {
    public UUID userId;
    public double longitude;
    public double latitude;

    public VisitedLocationDto() {
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
